<!DOCTYPE html>
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #43484c;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .wh-30{
        width: 30%;
    }

    table, th, td {
        border: 1px solid black;
    }
</style>

    <head>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th class="wh-30">Title</th>
                            <th>Post Id</th>
                            <th class="wh-30">Paragraph</th>
                            <th>Owner</th>
                            <th>Address</th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (!empty($posts_data))
                        @foreach($posts_data as $post_data)
                            <tr>
                                <td>{{$post_data->id}}</td>
                                <td>{{$post_data->title}}</td>
                                <td>{{$post_data->postExtAttributes[0]->post_id}}</td>
                                <td>{{$post_data->postExtAttributes[0]->paragraph}}</td>
                                <td>{{$post_data->postExtAttributes[0]->owner}}</td>
                                <td>{{$post_data->postExtAttributes[0]->address}}</td>
                                <td>{{$post_data->postExtAttributes[0]->created_at}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
        </div>
    </body>
</html>
