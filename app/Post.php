<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $date = ['created_at', 'updated_at'];
    protected $fillable = ['title', 'type_of_post'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postExtAttributes()
    {
        return $this->hasMany('App\PostExtAttributes');
    }
}
