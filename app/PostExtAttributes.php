<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostExtAttributes extends Model
{
    protected $date = ['created_at', 'updated_at'];
    protected $fillable = ['title', 'type_of_post'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function postExtAttributes()
    {
        return $this->belongsTo('App\Post');
    }
}
