<?php


namespace App\Http\Controllers;

use App\Post;
use App\PostExtAttributes;

class PostController extends Controller
{

    public function getPostData(){

//        может неправильно работать в этом случае, поскольку я добавляю информацию в таблицы с помощью seed-ов поэтому
//        дата created_at почти у всех совпадает(если информация будет добавляться в ручную то будет работать без проблем).
        $posts_data = Post::with('postExtAttributes')
            ->orderBy('created_at', 'desc')
            ->limit(100)
            ->get();

//        может неправильно работать в этом случае, поскольку я добавляю информацию в таблицы с помощью seed-ов поэтому
//        дата created_at почти у всех совпадает(если информация будет добавляться в ручную то будет работать без проблем).
        $posts_data = Post::with('postExtAttributes')
            ->latest()
            ->take(100)
            ->get();

//
        $posts_data = Post::with('postExtAttributes')
            ->orderBy('id', 'desc')
            ->take(100)
            ->get();

        return view('welcome', compact('posts_data'));
    }
}
